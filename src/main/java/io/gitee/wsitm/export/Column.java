package io.gitee.wsitm.export;


import java.lang.reflect.Method;
import java.util.Map;


public class Column {

    public Column() {
    }

    public Column(String key, Class<?> clazz, int sort, String[] headers, int width) {
        this.key = key;
        this.clazz = clazz;
        this.sort = sort;
        this.headers = headers;
        this.width = width;
    }

    /**
     * 字段名
     */
    private String key;

    /**
     * 字段类型
     */
    private Class<?> clazz;

    /**
     * 排序
     */
    private int sort;

    /**
     * 元素表头
     */
    private String[] headers;

    /**
     * 处理填充表头
     */
    private String[] fullHeaders;

    /**
     * 列宽
     */
    private int width;

    /**
     * 枚举翻译
     */
    private Map<String, String> converterMap;

    /**
     * 时间格式化
     */
    private String dateFormat;


    /**
     * 文字前缀，如$ 90 变成$90
     */
    public String prefix = "";

    /**
     * 文字后缀，如% 90 变成90%
     */
    public String suffix = "";


    /**
     * 百分比化，大于等于0表示百分比后保留的位数，如 0.864357，填2时，变成 86.44%；填0时，变成 86%
     */
    public int percent = -1;


    private Object handlerIns;

    private Method handlerMethod;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String[] getHeaders() {
        return headers;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public String[] getFullHeaders() {
        return fullHeaders;
    }

    public void setFullHeaders(String[] fullHeaders) {
        this.fullHeaders = fullHeaders;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Map<String, String> getConverterMap() {
        return converterMap;
    }

    public void setConverterMap(Map<String, String> converterMap) {
        this.converterMap = converterMap;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }


    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public Object getHandlerIns() {
        return handlerIns;
    }

    public void setHandlerIns(Object handlerIns) {
        this.handlerIns = handlerIns;
    }

    public Method getHandlerMethod() {
        return handlerMethod;
    }

    public void setHandlerMethod(Method handlerMethod) {
        this.handlerMethod = handlerMethod;
    }
}
