package io.gitee.wsitm.export;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.*;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.excel.cell.CellUtil;
import io.gitee.wsitm.exception.ClutterException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 数据导出工具
 *
 * @author wsitm
 * @date 2023/9/23 9:34
 */
@SuppressWarnings({"all"})
public class ExportKit {
    /**
     * 导出 excel ，不适合导出大文件
     *
     * @param response 响应对象
     * @param list     数据
     */
    public static <T> void exportXlsx(HttpServletResponse response, List<T> list) {
        String curTime = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
        exportXlsx(response, String.format("数据-%s.xlsx", curTime), list);
    }

    /**
     * 导出 excel ，不适合导出大文件
     *
     * @param response 响应对象
     * @param filename 文件名
     * @param list     数据
     */
    public static <T> void exportXlsx(HttpServletResponse response, String filename, List<T> list) {
        try {
            filename = URLEncoder.encode(filename, "UTF-8");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + filename);
            exportXlsx(response.getOutputStream(), list);
        } catch (Exception e) {
            throw new ClutterException("导出异常", e);
        }
    }

    /**
     * 导出 excel ，不适合导出大文件
     *
     * @param dirPath  目录路径
     * @param fileName 文件名
     * @param list     数据
     */
    public static <T> File exportXlsx(String dirPath, String fileName, List<T> list) {
        File file = FileUtil.file(dirPath, fileName);
        exportXlsx(file, list);
        return file;
    }

    /**
     * 导出 excel ，不适合导出大文件
     *
     * @param file 文件
     * @param list 数据
     */
    public static <T> void exportXlsx(File file, List<T> list) {
        OutputStream outputStream = FileUtil.getOutputStream(file);
        exportXlsx(outputStream, list);
    }

    /**
     * 导出 excel ，不适合导出大文件
     *
     * @param outputStream 输出流
     * @param list         数据
     */
    public static <T> void exportXlsx(OutputStream outputStream, List<T> list) {
        try (ExcelWriter excelWriter = ExcelUtil.getBigWriter()) {
            exportXlsx(excelWriter, list);
            excelWriter.flush(outputStream, true);
        } finally {
            IoUtil.close(outputStream);
        }
    }

    /**
     * 导出 excel ，不适合导出大文件
     *
     * @param excelWriter excel写入对象
     * @param list        数据
     */
    public static <T> void exportXlsx(ExcelWriter excelWriter, List<T> list) {
        if (CollUtil.isNotEmpty(list)) {
            try {
                // 是否已经写入头部
                boolean hasHeader = false;
                // 头部信息
                ExportInfo exportInfo = null;
                for (int i = 0; i < list.size(); i++) {
                    T item = list.get(i);
                    if (item == null) {
                        continue;
                    }
                    if (ObjectUtil.isNull(exportInfo)) {
                        // 解析头部信息
                        exportInfo = getColumnList(item);
                    }

                    if (!hasHeader) {
                        hasHeader = true;
                        fullHeader(excelWriter, exportInfo);
                        if (item instanceof Map) {
                            // 如果是map数据类型，不再执行下面数据行
                            exportInfo.setHeaderRow(exportInfo.getHeaderRow() - 1);
                            continue;
                        }
                    }

                    // 渲染一行数据
                    List<Column> columnList = exportInfo.getColumnList();
                    Map<String, Object> map = BeanUtil.beanToMap(item, new LinkedHashMap<>(), false, false);
                    for (int j = 0; j < columnList.size(); j++) {
                        Column column = columnList.get(j);
                        Object obj = formatValue(map, column, item);
                        String value = ObjectUtil.isNotEmpty(obj) ? (column.getPrefix() + obj + column.getSuffix()) : "";
                        excelWriter.writeCellValue(j, i + exportInfo.getHeaderRow(), value);
                    }
                }
            } catch (Exception e) {
                throw new ClutterException("导出异常", e);
            }
        }
    }

    /**
     * 设置单元格
     *
     * @param excelWriter excel写入对象
     * @param x           x
     * @param y           x
     * @param text        内容
     * @param isHeader    头
     */
    public static void setCell(ExcelWriter excelWriter, int x, int y, Object text, boolean isHeader) {
        Row row = excelWriter.getOrCreateRow(y);
        Cell cell = CellUtil.getOrCreateCell(row, x);
        CellUtil.setCellValue(cell, ObjectUtil.defaultIfNull(text, ""), excelWriter.getStyleSet(), isHeader);
    }

    /**
     * 填充表头
     *
     * @param writer     excel写入对象
     * @param exportInfo 列信息
     */
    protected static void fullHeader(ExcelWriter writer, ExportInfo exportInfo) {
        int headerRow = exportInfo.getHeaderRow();
        List<Column> columnList = exportInfo.getColumnList();
        List<Merge> mergeList = new ArrayList<>();
        for (int i = 0; i < columnList.size(); i++) {
            String[] fullHeaders = columnList.get(i).getFullHeaders();
            int width = columnList.get(i).getWidth();
            if (width <= 0) {
                width = fullHeaders.length > 0 && StrUtil.isNotBlank(fullHeaders[0]) ? fullHeaders[0].length() * 3 : 15;
            }
            writer.setColumnWidth(i, width);

            if (!columnList.get(i).getClazz().isAssignableFrom(Number.class)) {
                CellStyle cellStyle = writer.getCellStyle();
                DataFormat dataFormat = writer.getWorkbook().createDataFormat();
                cellStyle.setDataFormat(dataFormat.getFormat("@"));
                writer.setColumnStyle(i, cellStyle);
            }

            for (int j = 0; j < fullHeaders.length; j++) {
                // 逐个插入表头
                Row row = writer.getOrCreateRow(j);
                Cell cell = CellUtil.getOrCreateCell(row, i);
                CellUtil.setCellValue(cell, ObjectUtil.defaultIfNull(fullHeaders[j], ""), writer.getStyleSet(), true);

                // 判断获取合并的坐标
                if (CollUtil.isEmpty(mergeList)) {
                    mergeList.add(new Merge(fullHeaders[j], i, j));
                } else {
                    final int x = i, y = j;
                    Optional<Boolean> optionalBoolean = mergeList.stream()
                            .map(merge -> merge.merge(fullHeaders[y], x, y))
                            .filter(Boolean::booleanValue)
                            .findFirst();
                    if (!optionalBoolean.isPresent()) {
                        mergeList.add(new Merge(fullHeaders[j], i, j));
                    }
                }
            }
        }

        // 合并表头
        for (Merge merge : mergeList) {
            if (merge.isMerge()) {
                writer.merge(merge.getStartRowIndex(), merge.getEndRowIndex(),
                        merge.getStartColIndex(), merge.getEndColIndex(), null, true);
            }
        }
        // 跳到非表头行
        if (CollUtil.isNotEmpty(columnList)) {
            writer.setCurrentRow(headerRow);
        }

    }

    // --------------------------------------------------------------------------------------------------------------------

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param response 响应对象
     * @param consumer 消费者，流式回调
     */
    public static <T> void exportCsv(HttpServletResponse response,
                                     Consumer<FlushFunc<T, Integer>> consumer) {
        String curTime = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
        exportCsv(response, String.format("data-%s.csv", curTime), consumer);
    }

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param response 响应对象
     * @param filename 文件名，默认拼接当前时间，和文件后缀
     * @param consumer 消费者，流式回调
     */
    public static <T> void exportCsv(HttpServletResponse response, String filename,
                                     Consumer<FlushFunc<T, Integer>> consumer) {
        exportCsv(response, filename, ",", Charset.forName("gbk"), true, consumer);
    }

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param response  响应对象
     * @param filename  文件名，默认拼接当前时间，和文件后缀
     * @param delimiter 分隔符
     * @param charset   字符集
     * @param consumer  消费者，流式回调
     */
    public static <T> void exportCsv(HttpServletResponse response, String filename,
                                     String delimiter, Charset charset, Boolean withTab, Consumer<FlushFunc<T, Integer>> consumer) {
        try (
                OutputStream outputStream = response.getOutputStream()
        ) {
            filename = URLEncoder.encode(filename, "UTF-8");
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + filename);
            exportCsv(outputStream, delimiter, charset, withTab, consumer);
            outputStream.flush();
        } catch (Exception e) {
            throw new ClutterException("导出异常", e);
        }
    }

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param dirPath  目录路径
     * @param fileName 文件名
     * @param consumer 消费者，流式回调
     * @return 文件
     */
    public static <T> File exportCsv(String dirPath, String fileName,
                                     Consumer<FlushFunc<T, Integer>> consumer) {
        File file = FileUtil.file(dirPath, fileName);
        exportCsv(file, consumer);
        return file;
    }

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param file     导出文件
     * @param consumer 消费者，流式回调
     * @throws ClutterException 异常
     */
    public static <T> void exportCsv(File file, Consumer<FlushFunc<T, Integer>> consumer) {
        OutputStream outputStream = FileUtil.getOutputStream(file);
        exportCsv(outputStream, ",", Charset.forName("gbk"), true, consumer);
    }

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param outputStream 输出流
     * @param consumer     消费者，流式回调
     * @throws ClutterException 异常
     */
    public static <T> void exportCsv(OutputStream outputStream, Consumer<FlushFunc<T, Integer>> consumer) {
        exportCsv(outputStream, ",", Charset.forName("gbk"), true, consumer);
    }

    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param outputStream 输出流
     * @param delimiter    分隔符
     * @param charset      字符集
     * @param withTab      是否带有制表符，方便文本呈现
     * @param consumer     消费者，流式回调
     */
    public static <T> void exportCsv(OutputStream outputStream, String delimiter, Charset charset, Boolean withTab,
                                     Consumer<FlushFunc<T, Integer>> consumer) {
        exportCsv(outputStream, delimiter, charset, withTab, true, consumer);
    }


    /**
     * 导出CSV文件，使用分段式，减少内存的使用，大量数据时建议使用该方式
     *
     * @param outputStream            输出流
     * @param delimiter               分隔符
     * @param charset                 字符集
     * @param withTab                 是否带有制表符，方便文本呈现
     * @param withDoubleQuotationMark 是否带有双引号，字符串类型数据带有双引号
     * @param consumer                消费者，流式回调
     */
    public static <T> void exportCsv(OutputStream outputStream, String delimiter, Charset charset, Boolean withTab,
                                     Boolean withDoubleQuotationMark, Consumer<FlushFunc<T, Integer>> consumer) {
        try {
            String tab = withTab ? "\t" : "";
            AtomicBoolean hasHeader = new AtomicBoolean(false);
            AtomicReference<ExportInfo> atomicReference = new AtomicReference<>();
            consumer.accept((list) -> {
                if (ArrayUtil.isNotEmpty(list)) {
                    try {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (T item : list) {
                            if (item == null) continue;

                            ExportInfo exportInfo = atomicReference.get();
                            if (ObjectUtil.isNull(exportInfo)) {
                                exportInfo = getColumnList(item);
                                atomicReference.set(exportInfo);
                            }

                            List<Column> columnList = exportInfo.getColumnList();
                            if (!hasHeader.getAndSet(true)) {
                                String strHeader = columnList.stream()
                                        .map(column -> {
                                            String header = ArrayUtil.join(column.getHeaders(), "-");
                                            String format = getCsvFormat(header, withDoubleQuotationMark) + tab;
                                            return String.format(format, header);
                                        })
                                        .collect(Collectors.joining(delimiter));
                                outputStream.write((strHeader + "\r\n").getBytes(charset));
                                outputStream.flush();
                                if (item instanceof Map) {
                                    // 如果是map数据类型，不再执行下面数据行
                                    continue;
                                }
                            }

                            Map<String, Object> map = BeanUtil.beanToMap(item, new LinkedHashMap<>(), false, false);
                            String strLine = columnList.stream()
                                    .map(column -> {
                                        Object obj = formatValue(map, column, item);
                                        String format = getCsvFormat(obj, withDoubleQuotationMark) + tab;
                                        String value = ObjectUtil.isNotEmpty(obj) ?
                                                (column.getPrefix() + obj + column.getSuffix()) : "";
                                        return String.format(format, value);
                                    })
                                    .collect(Collectors.joining(delimiter));
                            stringBuilder.append(strLine).append("\r\n");
                        }

                        outputStream.write(stringBuilder.toString().getBytes(charset));
                        outputStream.flush();

                        return list.length;
                    } catch (Exception e) {
                        throw new ClutterException("导出异常", e);
                    }
                }
                return 0;
            });
        } finally {
            IoUtil.close(outputStream);
        }
    }

    // --------------------------------------------------------------------------------------------------------------------

    private static String getCsvFormat(Object value, Boolean withDoubleQuotationMark) {
        return value instanceof Number || !Boolean.TRUE.equals(withDoubleQuotationMark) ? "%s" : "\"%s\"";
    }

    /**
     * 格式化数据
     *
     * @param map    转化后的数据
     * @param column 列信息
     * @param item   原数据
     * @param <T>    泛型
     * @return 格式化后的数据
     */
    protected static <T> Object formatValue(Map<String, Object> map, Column column, T item) {
        Object obj = map.get(column.getKey());

        if (CollUtil.isNotEmpty(column.getConverterMap())) {
            // 进行翻译
            obj = column.getConverterMap().get(Objects.toString(obj));
        }

        //时间格式化
        if (StrUtil.isNotEmpty(column.getDateFormat())) {
            obj = formatDateTime(obj, column.getDateFormat());
        }

        if (column.getHandlerIns() != null && column.getHandlerMethod() != null) {
            try {
                // 数据处理器
                obj = column.getHandlerMethod().invoke(column.getHandlerIns(), obj, item);
            } catch (Exception e) {
                throw new ClutterException("不能格式化数据", e);
            }
        }

        if (obj instanceof Number && column.getPercent() > -1) {
            obj = String.format("%." + column.getPercent() + "f%%", Convert.toDouble(obj) * 100);
        }

        return obj;
    }


    /**
     * 通过对象获取列，根据 @Header 判断
     *
     * @param object 对象
     * @return 列
     */
    protected static synchronized ExportInfo getColumnList(Object object)
            throws InstantiationException, IllegalAccessException, NoSuchMethodException {
        if (object instanceof List) {
            throw new ClutterException("元素对象不能是List");
        }

        AtomicInteger headerRow = new AtomicInteger(1);
        List<Column> columnList = new ArrayList<>();
        if (object instanceof Map) {
            int order = 0;
            Map<String, Object> map = BeanUtil.beanToMap(object);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                columnList.add(new Column(StrUtil.toString(key),
                        value.getClass(), ++order, new String[]{StrUtil.toString(value)}, 0));
            }
        } else {
            Class<?> beanClass = ClassUtil.getClass(object);
            // 获取所有字段
            Field[] fields = ReflectUtil.getFields(beanClass, field -> field.getAnnotation(Header.class) != null);

            if (fields == null || fields.length == 0) {
                throw new ClutterException("解析字段为空，是否已经引用@Header注解");
            }

            for (Field field : fields) {
                Header header = field.getAnnotation(Header.class);
                Column column = new Column(field.getName(), field.getType(),
                        header.sort(), header.value(), header.width());

                if (StrUtil.isNotEmpty(header.dateFormat())) {
                    column.setDateFormat(header.dateFormat());
                }

                if (StrUtil.isNotEmpty(header.converterExp())) {
                    List<String> expList = StrUtil.split(header.converterExp(), ",");
                    if (CollUtil.isNotEmpty(expList)) {
                        Map<String, String> expMap = expList.stream()
                                .map(str -> {
                                    List<String> keyVal = StrUtil.split(str, "=");
                                    if (CollUtil.isNotEmpty(keyVal) && keyVal.size() > 1) {
                                        return keyVal;
                                    }
                                    return null;
                                })
                                .filter(list -> CollUtil.isNotEmpty(list) && StrUtil.isNotEmpty(list.get(0)))
                                .collect(Collectors.toMap(
                                        list -> list.get(0).trim(),
                                        list -> list.get(1).trim(),
                                        (m1, m2) -> m2
                                ));
                        column.setConverterMap(expMap);
                    }
                }

                if (!header.handler().equals(HandlerAdapter.class)) {
                    column.setHandlerIns(header.handler().newInstance());
                    column.setHandlerMethod(header.handler().getMethod("format", new Class[]{Object.class, beanClass}));
                }

                column.setPrefix(header.prefix());
                column.setSuffix(header.suffix());
                column.setPercent(header.percent());

                columnList.add(column);
                if (header.value().length > headerRow.get()) {
                    headerRow.set(header.value().length);
                }
            }
        }

        // 处理表头，进行排序
        columnList = columnList.stream()
                .peek(column -> {
                    String[] headers = column.getHeaders();
                    if (headers.length != headerRow.get()) {
                        String[] newHeaders = new String[headerRow.get()];
                        System.arraycopy(headers, 0, newHeaders, 0, headers.length);
                        for (int i = headers.length; i < headerRow.get(); i++) {
                            newHeaders[i] = headers[headers.length - 1];
                        }
                        column.setFullHeaders(newHeaders);
                    } else {
                        column.setFullHeaders(headers);
                    }
                })
                .sorted(Comparator.comparingInt(Column::getSort))
                .collect(Collectors.toList());

        return new ExportInfo(headerRow.get(), columnList);
    }

    // -------------------------------------------------------------------------------


    /**
     * 格式化时间
     *
     * @param obj    值
     * @param format 格式
     * @return 格式后时间
     */
    public static String formatDateTime(Object obj, String format) {
        if (obj instanceof Date) {
            return DateUtil.format((Date) obj, format);
        } else if (obj instanceof LocalDateTime) {
            return DateUtil.format((LocalDateTime) obj, format);
        } else if (obj instanceof LocalDate) {
            return DateUtil.format(((LocalDate) obj).atStartOfDay(), format);
        } else if (obj instanceof String) {
            return DateUtil.parse(obj.toString()).toString(format);
        } else if (obj instanceof Long) {
            return DateUtil.date((long) obj).toString(format);
        }
        return StrUtil.toString(obj);
    }

    static class ExportInfo {
        private Integer headerRow;
        private List<Column> columnList;

//        private String delimiter;
//        private Charset charset;
//        private Boolean withTab;

        public ExportInfo() {
        }

        public ExportInfo(Integer headerRow, List<Column> columnList) {
            this.headerRow = headerRow;
            this.columnList = columnList;
        }

        public Integer getHeaderRow() {
            return headerRow;
        }

        public void setHeaderRow(Integer headerRow) {
            this.headerRow = headerRow;
        }

        public List<Column> getColumnList() {
            return columnList;
        }

        public void setColumnList(List<Column> columnList) {
            this.columnList = columnList;
        }
    }

    @FunctionalInterface
    public static interface FlushFunc<T, R> {
        R apply(T... t);
    }
}
