package io.gitee.wsitm.export;

public class Merge {

    /**
     * 合并的名称
     */
    private String name;
    /**
     * 是否做合并
     */
    private boolean isMerge;
    /**
     * 开始列
     */
    private Integer startColIndex;
    /**
     * 结束列
     */
    private Integer endColIndex;
    /**
     * 开始行
     */
    private Integer startRowIndex;
    /**
     * 结束行
     */
    private Integer endRowIndex;

    public Merge(String name, int x, int y) {
        this.name = name;
        this.isMerge = false;
        this.startColIndex = x;
        this.endColIndex = x;
        this.startRowIndex = y;
        this.endRowIndex = y;
    }

    /**
     * 合并，判断是否符合合并且做合并动作
     *
     * @param name 合并名
     * @param x    坐标X
     * @param y    左边Y
     * @return 是否合并成功
     */
    public boolean merge(String name, int x, int y) {
        if (name.equals(this.name)) {
            boolean flag = false;
            if (this.endColIndex + 1 == x) {
                this.endColIndex = x;
                this.isMerge = true;
                flag = true;
            }
            if (this.endRowIndex + 1 == y) {
                this.endRowIndex = y;
                this.isMerge = true;
                flag = true;
            }
            return flag;
        }
        return false;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMerge() {
        return isMerge;
    }

    public void setMerge(boolean merge) {
        isMerge = merge;
    }

    public Integer getStartColIndex() {
        return startColIndex;
    }

    public void setStartColIndex(Integer startColIndex) {
        this.startColIndex = startColIndex;
    }

    public Integer getEndColIndex() {
        return endColIndex;
    }

    public void setEndColIndex(Integer endColIndex) {
        this.endColIndex = endColIndex;
    }

    public Integer getStartRowIndex() {
        return startRowIndex;
    }

    public void setStartRowIndex(Integer startRowIndex) {
        this.startRowIndex = startRowIndex;
    }

    public Integer getEndRowIndex() {
        return endRowIndex;
    }

    public void setEndRowIndex(Integer endRowIndex) {
        this.endRowIndex = endRowIndex;
    }
}
