package io.gitee.wsitm.export;

import java.lang.annotation.*;

/**
 * 导出 表头注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Header {

    /**
     * 表头
     *
     * @return 数组
     */
    String[] value();

    /**
     * 导出时在excel中排序
     */
    int sort() default Integer.MAX_VALUE / 2;

    /**
     * 列宽，excel文件有效，0、表示自动
     *
     * @return 宽度
     */
    int width() default 0;

    /**
     * 文字前缀，如$ 90 变成$90
     */
    public String prefix() default "";

    /**
     * 文字后缀，如% 90 变成90%
     */
    public String suffix() default "";


    /**
     * 百分比化，大于等于0表示百分比后保留的位数，
     * 如 0.864357，填2时，变成 86.44%；填0时，变成 86%
     */
    public int percent() default -1;


    /**
     * 转换器，格式："key1=value1,key2=value2"，翻译枚举
     **/
    String converterExp() default "";

    /**
     * 日期格式，如: yyyy-MM-dd
     */
    public String dateFormat() default "";

    /**
     * 自定义数据处理器
     */
    public Class<?> handler() default HandlerAdapter.class;

}
