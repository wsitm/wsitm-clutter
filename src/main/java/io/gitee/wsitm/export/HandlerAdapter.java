package io.gitee.wsitm.export;

/**
 * Excel数据格式处理适配器
 *
 * @author ruoyi
 */
public interface HandlerAdapter<T> {
    /**
     * 格式化
     *
     * @param value 单元格数据值
     * @param cell  元素值
     * @return 处理后的值
     */
    Object format(Object value, T cell);
}
