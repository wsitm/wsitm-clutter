package io.gitee.wsitm.exception;

import cn.hutool.core.exceptions.ExceptionUtil;

/**
 * 运行时异常
 *
 * @author wsitm
 */
public class ClutterException extends RuntimeException {
    private static final long serialVersionUID = 8247610319171014183L;

    public ClutterException(Throwable e) {
        super(ExceptionUtil.getMessage(e), e);
    }

    public ClutterException(String message) {
        super(message);
    }

    public ClutterException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * 导致这个异常的异常是否是指定类型的异常
     *
     * @param clazz 异常类
     * @return 是否为指定类型异常
     */
    public boolean causeInstanceOf(Class<? extends Throwable> clazz) {
        final Throwable cause = this.getCause();
        return null != clazz && clazz.isInstance(cause);
    }
}
