package io.gitee.wsitm.test;

import cn.hutool.core.lang.Dict;
import io.gitee.wsitm.export.ExportKit;
import io.gitee.wsitm.export.Header;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

public class ExportTest {

    @Test
    public void test() {
        Demo vo = new Demo("1", "测试", "地址地址地址地址", null, null, 0.234234d);

        ExportKit.exportCsv("./file/temp", "test.csv", (func) -> {

            func.apply(vo);
        });
    }

    @Test
    public void test2() {
        Dict header = Dict.create().set("aaa", "测试1").set("bbb", "测试2").set("ccc", "测试3");
        Dict data = Dict.create().set("aaa", "123").set("bbb", "234").set("ccc", "456");

        ExportKit.exportCsv("./file/temp", "test2.csv", (func) -> {
            func.apply(header, data);
        });
    }

    @Test
    public void test3() {
        Demo vo = new Demo("1", "测试", "地址地址地址地址", null, null, 0.234234d);
        ExportKit.exportXlsx("./file/temp", "test.xlsx", Collections.singletonList(vo));
    }

    @Test
    public void test4() {
        Dict header = Dict.create().set("aaa", "测试1").set("bbb", "测试2").set("ccc", "测试3");
        Dict data = Dict.create().set("aaa", "123").set("bbb", "234").set("ccc", "456");
        ExportKit.exportXlsx("./file/temp", "test2.xlsx", Arrays.asList(header, data));
    }


    static class Demo {

        public Demo() {
        }

        public Demo(String id, String name, String address, String account, String phone, Double count) {
            this.id = id;
            this.name = name;
            this.address = address;
            this.account = account;
            this.phone = phone;
            this.count = count;
        }

        @Header(value = "楼宇ID")
        private String id;

        @Header(value = "楼宇名称")
        private String name;

        @Header(value = "详细地址", prefix = "地址")
        private String address;

        @Header(value = "账号")
        private String account;

        @Header(value = "电话")
        private String phone;

        @Header(value = "数据", percent = 2)
        private Double count;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Double getCount() {
            return count;
        }

        public void setCount(Double count) {
            this.count = count;
        }
    }
}
