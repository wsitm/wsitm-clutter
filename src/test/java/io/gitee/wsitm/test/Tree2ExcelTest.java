package io.gitee.wsitm.test;

import com.alibaba.fastjson2.JSONArray;
import io.gitee.wsitm.export.Tree2ExcelKit;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class Tree2ExcelTest {


    @Test
    public void test() throws IOException {
        String jsonStr = "[{\"name\":\"aaa\",\"children\":[{\"name\":\"bbb\",\"children\":[{\"name\":\"eee\"},{\"name\":\"fff\",\"children\":[{\"name\":\"iii\"},{\"name\":\"jjj\",\"children\":[{\"name\":\"qqq\"},{\"name\":\"ttt\"}]}]},{\"name\":\"www\"}]},{\"name\":\"ccc\",\"children\":[{\"name\":\"ggg\"},{\"name\":\"hhh\",\"children\":[{\"name\":\"kkk\",\"children\":[{\"name\":\"ttt\"},{\"name\":\"mmm\"}]},{\"name\":\"uuu\"}]},{\"name\":\"ooo\"}]},{\"name\":\"ddd\",\"children\":[{\"name\":\"ggg\"},{\"name\":\"hhh\",\"children\":[{\"name\":\"kkk\"},{\"name\":\"uuu\"}]}]}]}]";

        List<TreeE> list = JSONArray.parseArray(jsonStr, TreeE.class);

        String path = "C:\\Users\\LZY\\Desktop\\" + System.currentTimeMillis() + ".xls";

        File file = new File(path);
        file.createNewFile();

        Workbook workbook = new HSSFWorkbook();

        Tree2ExcelKit.handle(workbook, list, "上对齐", Tree2ExcelKit.Type.TOP,
                null, null, 1, 1, 1);
        Tree2ExcelKit.handle(workbook, list, "中对齐", Tree2ExcelKit.Type.MIDDLE,
                null, null, 1, 1, 1);
        Tree2ExcelKit.handle(workbook, list, "下对齐", Tree2ExcelKit.Type.BOTTOM,
                null, null, 1, 1, 1);

        try (
                FileOutputStream fos = new FileOutputStream(file)
        ) {
            workbook.write(fos);

            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workbook.close();
        }

    }

    static class TreeE {
        private String name;
        private List<TreeE> children;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<TreeE> getChildren() {
            return children;
        }

        public void setChildren(List<TreeE> children) {
            this.children = children;
        }
    }

}
